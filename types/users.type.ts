type IUser = {
    username: string;
    birthdate: string;
    email: string;
    role: string;
    createdAt?: string;
    updatedAt?: string;
    profilePicture?: string;
    bannerPicture?: string;
}

export type { IUser };